from django.db import models

# Create your models here.
class Carrera(models.Model):
    carrera_estado=(
        ('suspendida','Suspendida'),
        ('ejecutandose','Ejecutandose'),
        ('rediseno','Rediseño')
    )
    id_carrera = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=50, null=False)
    estado_carrera=models.CharField(max_length=20, choices=carrera_estado, verbose_name='Estado de la carrera')

class Cuenta(models.Model):
    id_cuenta = models.AutoField(primary_key=True)
    correo = models.EmailField(max_length=50,unique=True, null=False)

class Oferta_Laboral(models.Model):
    id_ofertas : models.AutoField(primary_key=True)
    
    id_cuenta = models.ForeignKey(
		'Cuenta',
		on_delete=models.CASCADE,
	)
    id_empresa = models.ForeignKey(
		'Empresa',
		on_delete=models.CASCADE,
	)

class Empresa(models.Model):
    id_empresa =models.AutoField(primary_key=True)
    nombre_empresa= models.CharField(max_length=50, null=False)
    sector = models.CharField(max_length=50, null=False)
    ubicacion = models.CharField(max_length=50, null=False)
    contacto = models.CharField(max_length=50, null=False)

class Encuentas_Laboral(models.Model):
    id_encuesta_laboral_satisfaccion =  models.AutoField(primary_key=True)   

class Informacion_laboral(models.Model):
    id_informacion_Oferta_laboral = models.AutoField(primary_key=True)
    cargo_ocupar =  models.CharField(max_length=50, null=False)
    remuneracion_economica =  models.IntegerField(max_length=50, null=False)
    actividades_desempenar =  models.TextField(max_length=50, null=False)
    ciudad =  models.CharField(max_length=50, null=False)
    oferta_laboral = models.ForeignKey(
		'Oferta_Laboral',
		on_delete=models.CASCADE,
	)

class Estudiantes(models.Model):
    lista_estado = (
        ('estudiante_egresado','Estudiante Egresado'),
        ('graduado','Graduado')
    )
    id_estudiantes= models.AutoField(primary_key=True)
    nombres = models.CharField(max_length=50, null=False)
    apellidos = models.CharField(max_length=50, null=False)
    telefono = models.CharField(max_length=10,unique=True)
    estado = models.CharField(max_length=20, choices=lista_estado, null=False)
    carrera=models.ForeignKey(
		'Carrera',
		on_delete=models.CASCADE,
	)
    login=models.ForeignKey(
		'Cuenta',
		on_delete=models.CASCADE,
	)
    
class Hoja_de_vida(models.Model):
    id_hoja_de_vida = models.AutoField(primary_key=True)
    informacion_personal = models.TextField(max_length=500, null=False)
    preferencias_laborables = models.TextField(max_length=500, null=False)
    instruccion_formal = models.TextField(max_length=500, null=False)
    idiomas =models.TextField(max_length=500, null=False) 
    capacitaciones_certificado = models.TextField(max_length=500, null=False)
    expereiencia_laboral = models.TextField(max_length=500, null=False)
    logros_personal = models.TextField(max_length=500, null=False)
    referencias_personal = models.TextField(max_length=500, null=False)
    estudiante=models.ForeignKey(
		'Estudiantes',
		on_delete=models.CASCADE,
	)
