from django.contrib import admin

# Register your models here.
from .models import Carrera, Cuenta, Oferta_Laboral, Empresa, Encuentas_Laboral, Informacion_laboral,Estudiantes, Hoja_de_vida 
# Register your models here.
admin.site.register(Estudiantes)
admin.site.register(Carrera)
admin.site.register(Cuenta)
admin.site.register(Hoja_de_vida)
admin.site.register(Oferta_Laboral)
admin.site.register(Empresa)
admin.site.register(Encuentas_Laboral)
admin.site.register(Informacion_laboral)
